import { AuthRoutes } from '../modules/auth/routes';
import { EventRoutes } from '../modules/event/routes';
import { MissingRoute } from '../modules/missing/routes';
import { HomeRoutes } from '../modules/home/routes';
import { profileRoutes } from '../modules/profile/routes';

export const routes = [

    ...HomeRoutes,

    ...MissingRoute,

    ...EventRoutes,

    ...AuthRoutes,

    ...profileRoutes
      
];