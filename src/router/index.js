import { createRouter, createWebHistory } from 'vue-router';
import { routes } from './routes';
import store from '../store';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

async function isLoggedIn(){

  //check is token exist and user object in store is not empty 
  let token = localStorage.getItem('event-token');

  let user = store.getters.getUser;
  
  if(!user) await store.dispatch('getUser');

  user = store.getters.getUser;

  return (token !== null && user !== null);

}

router.beforeEach(async (to, from, next) => {

  if(to.matched.length === 0){
    next({
      path: '/404'
    });
  }

  if (to.meta.hasOwnProperty('auth')) {
    if (! await isLoggedIn()) {
      
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    }

  } else if (to.meta.hasOwnProperty('guest')) {

    if (await isLoggedIn()) {

      next({
        path: "/",
        query: { redirect: to.fullPath }
      });
    } 

  }

  next();
});

export default router;
