import {state } from './state.js';
import { mutations } from './mutation.js';
import { actions } from './actions.js';
import { getters } from './getters.js';

const loaderModule = {
    state: state,
    actions : actions,
    mutations : mutations,
    getters : getters
}

export {loaderModule};