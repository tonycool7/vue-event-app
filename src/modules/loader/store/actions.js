export const actions = {
    setLoader({commit}, payload){
        commit('setLoading', payload);
    }
};