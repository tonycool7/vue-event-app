export const MissingRoute = [
    {
        path: '/404',
        name: 'Missing',
        component: () => import('./pages/Missing.vue')
    }
]