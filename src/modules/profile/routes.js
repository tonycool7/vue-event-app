export const profileRoutes = [
    {
        path: '/profile',
        name: 'profileParentComponent',
        component: () => import('./pages/Profile.vue')
    }
]