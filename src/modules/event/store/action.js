import { Http } from '../../../api/Http';
import router from '../../../router/index';

export const actions = {
    async loadEvents({ commit }){

        let response = await Http.get('/event', true);

        let { data } = response;

        commit('setEvents', data);

        router.push('/');
    },

    async createEvent({ commit }, formData){

        let response = await Http.post('/event', formData, true);

        let { data } = response;

        commit('updateEvents', data);

        router.push('/');
    }

}