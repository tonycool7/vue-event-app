import { mutations } from './mutations.js';
import { state } from './state.js';
import { actions } from './action.js';
import { getters } from './getters.js';

export const eventModule = {
    state : state,
    mutations : mutations,
    actions : actions,
    getters : getters
}