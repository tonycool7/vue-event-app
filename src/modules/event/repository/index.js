
import { Http } from '../../../api/Http';

//endpoints for event repository
const RESOURCE = '/event';
const SINGLE_EVENT = '/event/{id}';

export class EventRepository {
    constructor(){}

    static async getAll(){
        return await Http.get(RESOURCE);
    }

    static async getById(id){
        let url = SINGLE_EVENT.replace('{id}', id);

        return await Http.get(url);
    }

    static async getDanceEvents(form) {
        let url = "test";

        return await Http.get(url);
    }

    static async getConferences(type) {
        let url = `conference-${type}`;

        return await Http.get(url);
    }
}