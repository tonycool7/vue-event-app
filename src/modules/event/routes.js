export const EventRoutes = [
    {
        path: '/event/:id',
        name: 'Event',
        meta : {
          auth: true
        },
        component: () => import('./pages/Event.vue')
      }
];