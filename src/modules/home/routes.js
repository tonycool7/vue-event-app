export const HomeRoutes = [
    {
        path: '/',
        name: 'Home',
        meta : {
          auth: true
        },
        component: () => import('./pages/Home.vue')
    }
]