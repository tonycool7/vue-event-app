export const Fields = [
    {
        name : 'title',
        type : 'text',
        placeholder : 'Enter event title'
    },
    {
        name : 'type',
        type : 'hidden',
        value : 'event',
        placeholder : 'Enter event title'
    },
    {
        name : 'address',
        type : 'text',
        placeholder : 'Enter event address'
    },
    {
        name : 'country',
        type : 'text',
        placeholder : 'Host country'
    },
    {
        name : 'state',
        type : 'text',
        placeholder : 'State in country'
    },
    {
        name : 'src',
        type : 'file',
        placeholder : 'select event banner'
    },
    {
        name : 'start_time',
        type : 'datetime-local',
        placeholder : 'select start time'
    },
    {
        name : 'end_time',
        type : 'datetime-local',
        placeholder : 'select start time'
    },
]