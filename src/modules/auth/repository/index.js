
import { Http } from '../../../api/Http';

//endpoints for event repository
const RESOURCE = '/user';
const SINGLE_EVENT = '/event/{id}';

export class EventRepository {
    constructor(){}

    static async getAll(){
        return await Http.get(RESOURCE);
    }

    static async getById(id){
        let url = SINGLE_EVENT.replace('{id}', id);

        return await Http.get(url);
    }
}