import { mutations } from './mutations.js';
import { state } from './state.js';
import { actions } from './action.js';
import { getters } from './getters.js';

export const userModule = {
    state : state,
    mutations : mutations,
    actions : actions,
    getters : getters
}