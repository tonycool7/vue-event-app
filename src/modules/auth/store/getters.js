export const getters = {

    getErrorMsg(state){
        return state.errorMsg;
    },

    getUser(state){
        return state.user;
    },

    getErrors(state){
        return state.errors;
    }
}