import { Http } from '../../../api/Http';
import router from '../../../router/index';

export const actions = {
    async login({ commit }, formData){

        let response = await Http.post('/login', formData);

        if(response.hasOwnProperty('errors')) return;

        let { data } = response;

        localStorage.setItem('event-token', data.token);

        commit('setUser', data);

        router.push('/');

    },

    async register({ commit }, formData){

        let response = await Http.post('/register', formData);
        
        if(response.hasOwnProperty('errors')) return;

        let { data } = response;

        localStorage.setItem('event-token', data.token);
        
        commit('setUser', data);

        router.push('/');

    },

    async getUser({ commit }){

        let response = await Http.get('/user', true);

        if(response.hasOwnProperty('errors')){
            return null;
        }else{
            commit('setUser', response.data);
        }

    }

}