export const mutations = {
    setUser(state, payload){
        state.user = payload; 
    },

    setErrorMsg(state, payload){
        state.errorMsg = payload;
    },

    setErrors(state, payload){
        state.errors = payload;
    }
}