export const AuthRoutes = [
  {
    path: '/',
    name: 'AuthRoutes',
    component: () => import('./components/auth/Auth.vue'),
    children: [
      {
        path: 'login',
        name: 'Login',
        meta : {
          guest: true
        },
        component: () => import('./pages/Login.vue')
      },
    
      {
        path: 'register',
        name: 'Register',
        meta : {
          guest: true
        },
        component: () => import('./pages/Register.vue')
      }
    ]
  }
]