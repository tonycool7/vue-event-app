import { axiosClient } from './httpClient/AxiosClient';

export class Http{

    constructor(){}

    static setAuthHeader(){
        let access_token = localStorage.getItem('event-token');
        
        axiosClient.defaults.headers.common['Authorization'] = `Bearer ${access_token}`
    }

    static get(url, token = false){
        if(token){
            Http.setAuthHeader();
        }

        return axiosClient.get(url);
    }

    static post(url, data, token = false){
        if(token){
            Http.setAuthHeader();
        }

        return axiosClient.post(url, data);
    }

}