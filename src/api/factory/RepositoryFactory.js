
import { EventRepository } from '../../modules/event/repository';

let factories = {
    'event' : EventRepository,
};

export class RepositoryFactory{
    constructor(){}

    static get(name){
        if(!factories.hasOwnProperty(name)) throw "repository doesn't exist";

        return factories[name];
    }
}