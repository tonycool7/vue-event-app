import axios from 'axios';
import store from '../../store';

const axiosClient = axios.create({
  baseURL: "http://localhost:8000/v1"
});

axiosClient.defaults.withCredentials = true;

// intercepting responses
axiosClient.interceptors.response.use(function (response) {
    
    store.dispatch('setLoader', false);
  
    return Promise.resolve(response);

  }, function (error) {

    store.dispatch('setLoader', false);

    let {response} = error;

    let {status, data} = response;

    switch(status){
        case 401 :
            console.log("Unauthorized error");
            break;

        case 422 :
            console.log("Validation errors");
            store.commit('setErrorMsg', data.message);
            store.commit('setErrors', data.errors);
            break;

        default:
            break;
    }

    return data;
});


export { axiosClient };