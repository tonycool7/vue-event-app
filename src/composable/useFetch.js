import { ref, computed, reactive } from 'vue';
import axios from 'axios';

const access_token = localStorage.getItem('event-token');

const instance = axios.create({
    baseURL: 'http://localhost:8000/v1',
    headers: {'Authorization': `Bearer ${access_token}`}
  });

export const useFetch = (url, config = {}) => {
    
    let response = ref(null);
    let data = ref(null);
    let error = ref(null);
    let loading = ref(false);

    const fetch = async () => {
        loading = true;

        try{
            let result = await instance.request(
                `${api}/${url}`,
                ...config
            );

            response.value = result;
            data.value = result.data;

        }catch(e){
            error = e;
        }finally{
            loading = false;
        }   
    }

    !config.skip && fetch();

    return { response, data, error, loading, fetch }
}

const cacheMap = reactive(new Map());

export const useFetchCache = (key, url, config) => {

    const info = useFetch(url, { skip: true, ...config});

    const update = () => cacheMap.set(key, info.response.value);

    const clear = () => cacheMap.set(key, undefined);

    const fetch = async () => {
        try{
            await info.fetch();
            update();
        }catch{
            clear();
        }
    }

    const response = computed(() => cacheMap.get(key));

    const data = computed(() => response.value?.data);

    if(response.value == null) fetch();

    return { ...info, fetch, data, response, clear};

}