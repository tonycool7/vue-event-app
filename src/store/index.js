import { createStore } from 'vuex'
import { userModule } from '../modules/auth/store';
import { eventModule } from '../modules/event/store';
import { loaderModule } from '../modules/loader/store';

export default createStore({
  modules: {
    userModule,
    eventModule,
    loaderModule
  }
});
